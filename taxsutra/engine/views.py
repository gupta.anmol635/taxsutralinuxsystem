import dateutil.parser as dparser
import time
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render_to_response

import traceback
# import nltk
from nltk import word_tokenize, pos_tag, ne_chunk_sents
from nltk.tokenize import sent_tokenize
import os
from nltk.stem.wordnet import WordNetLemmatizer
import collections
import re
import collections

import docx2txt

import lxml.etree as etree
from django.conf import settings
import zipfile
try:
	from xml.etree.cElementTree import XML
except ImportError:
	from xml.etree.ElementTree import XML

LANGUAGE = "english"
SENTENCES_COUNT = 2
DELIMITER_OF_PARTITION = "||||"
PARENT_DIRECTORY = os.path.dirname(os.path.abspath(__file__))
NLP_STOPWORDS_EN = os.path.join(PARENT_DIRECTORY, 'resources/misc/stopwords-nlp-en.txt')

nltk_stemmer = WordNetLemmatizer()
with open(NLP_STOPWORDS_EN, 'r') as f:
	stop_words = set([w.strip() for w in f.readlines()])

split_head_body = ["O  R  D  E  R", "ORDER", "(OPEN COURT)", "JUDGEMENT","P.C.:","JUDGMENT","O R D E R", "J U D G M E N T", "J U D G E M E N T"]
footer_keys = ['  (JUDGE)','Sd/-']
text_title_dict = dict()
color_feature = {'yellow': 'issues', 'red': 'facts', 'green': 'decisions', 'blue': 'arguments_assessee', 'magenta': 'arguments_revenue', 'cyan': 'litigation_history'}

def join_format(is_fa_de):
	return " \n\n".join(is_fa_de) or "-"

def tag_freq(tags):
	text2 = " ".join(tags)
	counts = collections.Counter(text2.split())
	final_list = []
	for i in counts.most_common(5):
		if i[1]>1:
			final_list.append(i[0])
	return final_list

def extract_entity_names(t):
	entity_names = []
	if hasattr(t, 'node') and t.node:
		if t.node == 'NE':
			entity_names.append(' '.join([child[0] for child in t]))
		else:
			for child in t:
				entity_names.extend(extract_entity_names(child))
	else:
		if hasattr(t, 'label') and t.label():
			if t.label() == 'NE':
				entity_names.append(' '.join([child[0] for child in t]))
			else:
				for child in t:
					entity_names.extend(extract_entity_names(child))
	return entity_names

def overall_tag(text):
	if text in text_title_dict:
		return text_title_dict[text]
	text = text.replace("XX","")
	sentences = sent_tokenize(text)
	tokenized_sentences = [word_tokenize(sentence.strip()) for sentence in sentences]
	tagged_sentences = [pos_tag(sentence) for sentence in tokenized_sentences]
	chunked_sentences = ne_chunk_sents(tagged_sentences, binary=True) #nltk3

	entity_names = []
	for tree in chunked_sentences:
		entity_names.extend(extract_entity_names(tree))
	entity_names_set = list(set(entity_names))

	noun_tags = []
	for sentence in sentences:
		for word,pos in pos_tag(word_tokenize(str(sentence.lower()))):
			if word not in stop_words:
				 if (pos == 'NN' or pos == 'NNS' or pos == 'NNP'):
					 word = nltk_stemmer.lemmatize(word)
					 if len(word)>1 and word != 'page':
						 noun_tags.append(word)
	noun_tag_freq = tag_freq(noun_tags)

	overall_tags = noun_tag_freq + entity_names_set
	filtered_tags = [w for w in overall_tags if not w.lower() in stop_words]
	text_title_dict[text] = filtered_tags
	return filtered_tags

def check_and_get_grouped_text(is_fa_de, full_text):
	#improper text
	is_fa_de_new = []
	is_fa_de_temp = []
	index = 0
	while index < len(is_fa_de):
		temp = is_fa_de[index].strip()
		if temp:
			is_fa_de_temp.append(temp)
			try:
				groups = re.compile(re.sub(r'\s*','\W*', re.sub(r'\W*','',"".join(is_fa_de_temp)))[3:-3],re.IGNORECASE).search(full_text,re.IGNORECASE)
			except BaseException as e:
				print traceback.format_exc(e)
				return None
			if not groups:
				groups = re.compile(re.sub(r'\s*','\W*', re.sub(r'\W*','',"".join(is_fa_de_temp[0:-1])))[3:-3],re.IGNORECASE).search(full_text,re.IGNORECASE)
				if groups:
					is_fa_de_new.append(groups.group(0))
					is_fa_de_temp = []
					is_fa_de_temp.append(temp)
		index = index + 1

	if is_fa_de_temp:
		try:
			groups = re.compile(re.sub(r'\s*','\W*', re.sub(r'\W*','',"".join(is_fa_de_temp)))[3:-3],re.IGNORECASE).search(full_text,re.IGNORECASE)
		except BaseException as e:
			print traceback.format_exc(e)
			return None
		if groups:
			is_fa_de_new.append(groups.group(0))
	is_fa_de = is_fa_de_new

	is_fa_de_new = []
	for sent in is_fa_de:
		if re.search(r'\n\s*\d\W', sent, re.M):
			for s in re.split(r'\n\s*\d\W', sent):
				if s.strip():
					is_fa_de_new.append(s.strip())
		else:
			is_fa_de_new.append(sent)
	is_fa_de = is_fa_de_new

	is_fa_de_new = []
	sent2 = is_fa_de[0].strip()
	if len(sent2) > 1:
		sent2 = sent2[0].upper() + sent2[1:]
	is_fa_de_new.append(sent2)
	for i in range(0, len(is_fa_de) - 1):
		sent1 = is_fa_de[i].strip()
		sent2 = is_fa_de[i + 1].strip()
		try:
			intermediate = full_text[full_text.index(sent1) + len(sent1):full_text.index(sent2)]
		except BaseException as e:
			print "-"*20
			print full_text.find(sent2)
			print "-"*20
			print full_text.lower().find(sent2.lower())
			print "-"*20
		if ". " in intermediate:
			is_fa_de_new.append(".")
			if re.search(r'\n\d\W', intermediate, re.M) or re.search(r'\n\s+', intermediate, re.M):
				is_fa_de_new.append(DELIMITER_OF_PARTITION)
		if is_fa_de_new[-1][-1] in [".","?","!",DELIMITER_OF_PARTITION[-1]]:
			sent2 = sent2[0].upper() + sent2[1:]
		is_fa_de_new.append(sent2)
	is_fa_de = is_fa_de_new
	text = " ".join(is_fa_de).strip()
	return text

def clean_abbreviation(text):
	text = re.sub(r'[^\x00-\x7f]',r'', text)
	text = text.replace('U.S.', 'US').replace('U.K.', 'UK')
	text = text.replace('Jan.', 'Jan').replace('Feb.', 'Feb').replace('Mar.', 'Mar').replace('Apr.', 'Apr').replace('May.', 'May').replace('Jun.', 'Jun').replace('Jul.', 'Jul').replace('Aug.', 'Aug').replace('Sep.', 'Sep').replace('Oct.', 'Oct').replace('Nov.', 'Nov').replace('Dec.', 'Dec')
	text = text.replace('Sr. ', 'Sr ').replace('Mr. ', 'Mr ').replace('Dr. ', 'Dr ').replace('Prof. ', 'Prof ').replace('Mrs. ', 'Mrs ').replace('Smt. ', 'Smt ').replace('Er. ', 'Er ').replace('Pt. ', 'Pt ').replace('Esq. ', 'Esq ').replace('Hon. ', 'Hon ').replace('Jr. ', 'Jr ').replace('Ms. ', 'Ms ').replace('St. ', 'St ')
	text = text.replace('Gov. ', 'Gov ').replace('Govt. ', 'Govt ').replace('Pvt. ', 'Pvt ').replace('Ltd. ', 'Ltd ').replace('Ld. ', 'Ld ').replace('Inc. ', 'Inc ')
	text = text.replace('Rs.', 'Rs').replace('Co.', 'Co').replace('Retd.', 'Retd').replace('Re.', 'Re').replace('No.', 'No').replace('no.', 'no').replace(' rs.', ' Rs').replace(' ft.', ' ft')
	text = text.replace('i.e.', 'that is,').replace('e.g.', 'for example,').replace(' etc.', ' etc').replace(' no.', ' No').replace(' Md.', ' Md')
	text = text.replace("dtd.", "dated").replace("w.e.f.", "wef").replace("u/s.","u/s").replace("Sh.","Sh")
	text = re.sub(r'(?<!\w)([A-Z])\.', r'\1', text)
	# below code is for changing date format from 06.04.2018 to 06-04-2018
	text = re.sub(r'(\d+)\.(\d+)\.(\d+)', r"\1-\2-\3", text)
	text = re.sub(r"(?<=[.| ])([A-Z]{1})[.]", r"\1", text)
	# below code is for changing Sec. to section
	text = re.sub(r"sec[\.]?\s*(\d)", r"Section \1", text, flags=re.IGNORECASE)
	text = text.replace("ref. to", "ref to")
	try:
		text = text.replace('\xe2\x80\x9d','"').replace('\xe2\x80\x9c','"')
	except:
		try:
			text = text.decode('utf-8').replace('\xe2\x80\x9d','"').replace('\xe2\x80\x9c','"')
		except:
			pass
	text = re.sub(r'\s+\.', '.', text)
	return text.strip()

def clean_is_fa_de(is_fa_de, full_text):
	if not is_fa_de:
		return is_fa_de
	text = check_and_get_grouped_text(is_fa_de, full_text)
	#text = "".join(is_fa_de).strip()
	# below code is for removing characters not between 0-127 ascii
	text = clean_abbreviation(text)

	# below code is for removing last non fullstop to fullstop
	if len(text) > 10 and text[-1] != '.' and text[-1] != '?':
		for i in range(1, 7):
			end = text[-i]
			if end.isalpha() or end in [']',')','}']:
				text = text[0:len(text)-i+1] + "."
				break

	text = ' '.join(text.split())
	is_fa_de = text.split(DELIMITER_OF_PARTITION)
	is_fa_de = [i.strip() for i in is_fa_de if len(i) > 2]
	return is_fa_de

def coloured_document_extraction(path, text):
	print "----Coloured Document"
	WORD_NAMESPACE = '{http://schemas.openxmlformats.org/wordprocessingml/2006/main}'
	tag_rPr = WORD_NAMESPACE + 'rPr'
	tag_highlight = WORD_NAMESPACE + 'highlight'
	tag_val = WORD_NAMESPACE + 'val'
	tag_t = WORD_NAMESPACE + 't'
	document = zipfile.ZipFile(path)
	xml_content = document.read('word/document.xml')
	document.close()
	tree = etree.fromstring(xml_content)
	words = (tree.xpath('//w:r', namespaces=tree.nsmap))

	color_text = {}
	for color in color_feature.keys():
		color_text[color] = []

	for word in words:
		for rPr in word.findall(tag_rPr):
			high=rPr.findall(tag_highlight)

			for hi in high:
				color = hi.attrib[tag_val]
				if color and color in color_text:
					w = word.find(tag_t)
					if w is None:
						continue
					t = w.text
					if t and t.encode('utf-8').strip():
						t = t.encode('utf-8').strip()
						try:
							color_text[color].append(clean_abbreviation(t))
						except BaseException as e:
							print traceback.format_exc(e)
							continue

	judgement_data = {}
	for color in color_text.keys():
		is_fa_de_temp = color_text[color]
		is_fa_de_temp = clean_is_fa_de(is_fa_de_temp, text)
		is_fa_de_temp = join_format(is_fa_de_temp)
		judgement_data[color_feature[color]] = is_fa_de_temp

	return judgement_data

@csrf_exempt
def upload(request):
	if request.method != 'POST':
		return render_to_response('uploader.html', {'judgements': [], 'data': [], 'header_data': None, 'site_url':settings.SITE_URL}, context_instance=RequestContext(request))

	myfiles = request.FILES.getlist('docfiles')
	timestamp_suffix = "-" + str(int(time.time()))
	sheetname = myfiles[0].name.replace(".zip", "") + '.xls' if (len(myfiles) == 1 and ".zip" in myfiles[0].name) else 'judgements.xls'
	all_file_paths = get_all_files_from_zipped_file(myfiles, timestamp_suffix)

	if len(all_file_paths) == 0:
		return render_to_response('uploader.html', {'judgements': [], 'data': [], 'header_data': None, 'site_url':settings.SITE_URL}, context_instance=RequestContext(request))
	else:
		judgements = []
		for file_path in all_file_paths:
			judgements.append(one_judgement_at_a_time(file_path, timestamp_suffix))
		return render_to_response('uploader.html', {'sheetname': sheetname, 'judgements': judgements, 'site_url':settings.SITE_URL}, context_instance=RequestContext(request))

def get_all_files_from_zipped_file(myfiles, timestamp_suffix):
	all_file_paths = []
	for myfile in myfiles:
		myfile_name = myfile.name
		myfile_name = myfile_name.replace(" ","-")
		filename, file_extension = os.path.splitext(myfile_name)
		path = settings.DOC_ROOT + "/" + myfile_name.replace(file_extension, "") + "-" + timestamp_suffix
		file_extension = file_extension.lower()
		if ".docx" in file_extension:
			docpath = path + file_extension
			with open(docpath, 'w+') as f:
				f.write(myfile.file.read())
			all_file_paths.append(docpath)
		elif ".zip" in file_extension:
			directory = path
			if not os.path.exists(directory):
				os.makedirs(directory)
			zip_ref = zipfile.ZipFile(myfile)
			zip_ref.extractall(directory)
			zip_ref.close()
			for docpath in os.listdir(directory):
				if ".docx" in docpath:
					full_path = os.path.join(directory, docpath)
					if os.path.isfile(full_path):
						all_file_paths.append(full_path)
	return all_file_paths


def one_judgement_at_a_time(file_path, timestamp_suffix):
	header_data = {}
	data = []
	result = {}
	try:
		text = docx2txt.process(file_path)
		text = text.encode('utf-8').decode('ascii','ignore')

		result  = coloured_document_extraction(file_path, text)
		is_fa_de_body = ". \n\n".join(result.values())

		#-----------------SPLIT BODY---------------
		header = ""
		footer = ""
		body1 = ""
		for i in split_head_body:
			try:
				if i in text:
					new_text = text.split(i,1)
					header = new_text[0]
					body1 = new_text[1]
			except BaseException as e:
				print "1",traceback.format_exc(e)
				continue

		if not body1:
			body1 = text
			header = text

		for key in footer_keys:
			try:
				if key in body1:
					body2 = body1.split(key,1)
					body = body2[0]
					footer = body2[1]
			except BaseException as e:
				print "2", traceback.format_exc(e)
				continue


		header1 = []
		for line in header.splitlines():
			if line.strip():
				header1.append(line)
		header2 = "\n".join(header1)

		#--------------------extracting_from_head and body-------------------------
		keywords = []
		sections_index_list = set()
		appellante = ""
		respondante = ""
		sections_index = ""
		appellant_by = ""
		respondent_by = ""
		assessment_year = ""
		section_referred = ""
		assessee_by = ""
		revenue_by = ""
		courtname = ""
		judges = ""
		civil_no = ""
		date_of_judgemente = ""
		date_of_pronouncement = ""
		date_of_hearing = ""

		courtname_header_lines = header1[0:6] if len(header1) > 6 else header1
		courtname_header = "\n".join(courtname_header_lines)

		regexlist_courtname = [r'HIGH\s+COURT\s+(\w.*)', r'TRIBUNAL\s+COURT\s+(\w.*)', r'SUPREME\s+COURT\s+(\w.*)',r'THE\s+INCOME\s+TAX\W*(\w.*)',r'IN\s+THE\s+ITAT\W*(\w.*)']

		#-----------------extracting judges from head------------------------------
		for regex in regexlist_courtname:
			i = -1
			for x in header1:
				sh = re.search(regex, x, re.IGNORECASE)
				i = i + 1
				if sh:
					judges = header1[i+1]
					civil_no = header1[i+2]

		for regex in regexlist_courtname:
			s = re.search(regex, courtname_header, re.IGNORECASE)
			if s:
				courtname = s.group(0)
				if courtname:
					courtname = courtname.splitlines()[0]
				break
		for d in [" at "]:
			if d in courtname.lower():
				courtname = re.compile(d, re.IGNORECASE).split(courtname)[0]

		regexlist_date_of_hearing = [r'(D\s*a\s*t\s*e\s*o\s*f\s*H\s*e\s*a\s*r\s*i\s*n\s*g\D*[\d ,\/\-\.]+)', r'(D\s*a\s*t\s*e\s*o\s*f\s*C\s*o\s*n\s*c\s*l\s*u\s*d\s*i\s*n\s*g\s*t\s*h\s*e\s*H\s*e\s*a\s*r\s*i\s*n\s*g\D*[\d ,\/\-\.]+)']
		for regex in regexlist_date_of_hearing:
			date = re.search(regex, header2, re.IGNORECASE)
			if date:
				date = date.group(0).replace(' ', '')
				date = dparser.parse(date,fuzzy=True, dayfirst=True)
				if date:
					date_of_hearing = date.strftime("%d-%m-%Y")
					date_of_hearing = "'" + date_of_hearing
					break

		regexlist_date_of_pronouncement = [r'(D\s*a\s*t\s*e\s*o\s*f\s*O\s*r\s*d\s*e\s*r\D*[\d ,\/\-\.]+)',r'(P\s*r\s*o\s*n\s*o\s*u\s*n\s*c\s*e\s*d\s*o\s*n\D*[\d ,\/\-\.]+)',r'(D\s*a\s*t\s*e\s*o\s*f\s*P\s*r\s*o\s*n\s*o\s*u\s*n\s*c\D*[\d ,\/\-\.]+)']
		for regex in regexlist_date_of_pronouncement:
			date = re.search(regex, header2, re.IGNORECASE)
			if date:
				date = date.group(0).replace(' ', '')
				date = dparser.parse(date,fuzzy=True, dayfirst=True)
				if date:
					date_of_pronouncement = date.strftime("%d-%m-%Y")
					date_of_pronouncement = "'" + date_of_pronouncement
					break

		regexlist_date = [r'(DATE : \W*\w.*)',r'(Decided on: \W*\w.*)',r'(Judgment delivered on: \W*\w.*)',r'(Dated: \W*\w.*)',r'(Date of Reserving the Judgment \W*\w.*)',r'(DATED\s.*)']
		for regex in regexlist_date:
			date = re.search(regex, header2, re.IGNORECASE)
			if date:
				date = date.group(0).replace(' ', '')
				date = dparser.parse(date,fuzzy=True, dayfirst=True)
				if date:
					date_of_judgemente = date.strftime("%d-%m-%Y")
					date_of_judgemente = "'" + date_of_judgemente
					break

		regexlist_assessee_by = [r'By Assessee\W*(\w.*)',r'Assessee\s+by\W*(\w.*)',r'For Assessee\W*(\w.*)',r'Assessee by\s*:\n.*', r'For Assessee:\n\w.*']
		for regex_app in regexlist_assessee_by:
			rep_app = regex_app.replace("\W*","").replace("(\w.*)","").replace(".*(\w*)","").replace(".*","").replace("\w.*","").replace("\w","")
			assessee_by1 = re.search(regex_app, header2, re.IGNORECASE)
			if assessee_by1:
				assessee_by = re.compile(rep_app, re.IGNORECASE).sub("", assessee_by1.group(0)).replace(":","").replace("\n","").replace("..","")
				if "Respondent" in assessee_by:
					res_in_app = (re.search(r'Respondent by\W*(\w.*)', assessee_by, re.IGNORECASE)).group(0)
					assessee_by = assessee_by.replace(res_in_app,"")
				assessee_by = assessee_by.replace("with","").strip()
				break

		regexlist_revenue_by = [r'Revenue\s+by\W*(\w.*)',r'By Revenue\W*(\w.*)',r'For Revenue\W*(\w.*)',r'Revenue\s+by\s.*:\n.*',r'For Revenue :\n\w.*']
		for regex_res in regexlist_revenue_by:
			rep_res = regex_res.replace("\W*","").replace("(\w.*)","").replace(".*(\w*)","").replace("\s.*","").replace("\w.*","").replace(".*","")
			revenue_by1 = re.search(regex_res, header2, re.IGNORECASE)
			if revenue_by1:
				revenue_by = re.compile(rep_res, re.IGNORECASE).sub("", revenue_by1.group(0)).replace(":","").replace("\n","").replace("..","")
				revenue_by = revenue_by.replace("with","").strip()
				break

		regexlist_appellant_by = [r'Appellant by\W*(\w.*)',r'(\w.*)\W*for the Appellant',r'For the Appellant\W*.*',r'APPELLANT BY\s.*']
		for regex_app in regexlist_appellant_by:
			rep_app = regex_app.replace("\W*","").replace("(\w.*)","").replace(".*(\w*)","").replace(".*","").replace("\w.*","").replace("\w","")
			appellant_by1 = re.search(regex_app, header2, re.IGNORECASE)
			if appellant_by1:
				appellant_by = re.compile(rep_app, re.IGNORECASE).sub("", appellant_by1.group(0)).replace(":","").replace("\n","").replace("..","")
				if "respondent" in appellant_by.lower():
					groups = re.search(r'Respondent by\W*(\w.*)', appellant_by, re.IGNORECASE)
					if groups:
						res_in_app = groups.group(0)
						appellant_by = appellant_by.replace(res_in_app,"")
				appellant_by = appellant_by.replace("with","").strip()
				break

		regexlist_respondent_by = [r'Respondent by\W*(\w.*)', r'(\w.*)\W*for the Respondent',r'For the Respondent\W*.*',r'RESPONDENT BY\s.*']
		for regex_res in regexlist_respondent_by:
			rep_res = regex_res.replace("\W*","").replace("(\w.*)","").replace(".*(\w*)","").replace("\s.*","").replace("\w.*","").replace(".*","")
			respondent_by1 = re.search(regex_res, header2, re.IGNORECASE)
			if respondent_by1:
				respondent_by = re.compile(rep_res, re.IGNORECASE).sub("", respondent_by1.group(0)).replace(":","").replace("\n","").replace("..","")
				respondent_by = respondent_by.replace("with","").strip()
				respondent_by = re.sub("appellant", "", respondent_by, re.I)
				break

		regexlist_assessment_year = [r'Assessment Year\W*(\w.*)', r'(\w.*)\W*for the Assessment',r'For the Assessment\W*.*',r'ASSESSMENT YEAR\s.*']
		for regex_ass in regexlist_assessment_year:
			rep_ass = regex_ass.replace("\W*","").replace("(\w.*)","").replace(".*(\w*)","").replace("\s.*","").replace("\w.*","").replace(".*","")
			assessment_year1 = re.search(regex_ass, header2, re.IGNORECASE)
			if assessment_year1:
				assessment_year = re.compile(rep_ass, re.IGNORECASE).sub("", assessment_year1.group(0)).replace(":","").replace("\n","").replace("..","")
				assessment_year = assessment_year.replace("with","").strip()
				assessment_year = re.sub("assessment", "", assessment_year, re.I)
				break

		regexlist_section_referred = [r'Section Referred\W*(\w.*)', r'(\w.*)\W*for the Section',r'For the Section\W*.*',r'SECTION REFERRED\s.*']
		for regex_secr in regexlist_section_referred:
			rep_secr = regex_secr.replace("\W*","").replace("(\w.*)","").replace(".*(\w*)","").replace("\s.*","").replace("\w.*","").replace(".*","")
			section_referred1 = re.search(regex_secr, header2, re.IGNORECASE)
			if section_referred1:
				section_referred = re.compile(rep_secr, re.IGNORECASE).sub("", section_referred1.group(0)).replace(":","").replace("\n","").replace("..","")
				section_referred = section_referred.replace("with","").strip()
				section_referred = re.sub("section", "", section_referred, re.I)
				print section_referred
				break

		filename, file_extension = os.path.splitext(os.path.basename(file_path))
		filename = filename.replace(timestamp_suffix, "").strip()
		if filename.endswith("-") or filename.endswith("_"):
			filename = filename[0:-1].strip()

		if "-v-" not in filename:
			regexlist_appellant = [r'.*\W*(\w*).\.*.\s*.VS', r'.*\W*(\w*).\.*.\s*.versus', r'.*\W*VS',r'.*\W*vs.',r'1\.(.*?\n)*... Petitioners', r'\W*Appellant:\W*(\w.*)',r'.*\W*(\w*).\.*.\s*.Appellant', r'(\w+)\.*\s*Appellant']
			for regex in regexlist_appellant:
				rep = regex.replace("\W*","").replace("(\w.*)","").replace(".*(\w*)","").replace("\n","").replace(".*?","").replace("1\.","").replace("(\n)*","").replace(".*","")
				s = re.search(regex, header2, re.IGNORECASE)
				if s:
					appellante = re.compile(rep).sub("", s.group(0)).replace("..","").replace("Vs.","")
					appellante = re.sub(r'\s+vs[\.]{0,1}\s*','',appellante, flags=re.I).strip()
					appellante = re.compile(re.escape('with'), re.IGNORECASE).sub('', appellante).strip()
					for r in ['vs', 'versus', 'appellant']:
						if r in appellante.lower():
							appellante = re.compile(r, re.IGNORECASE).split(appellante)[0]
					break

			regexlist_respondant = [r'\WVS\s.*', r'\Wversus\s.*', r'v/s\n(\w.*)', r'vs\.\n(\w.*)', r'versus\.\n(\w.*)', r'Vs(.*?\n)*\... Respondents', r'\W*Respondent:\W*(\w.*)', r'.*\W*(\w*).\.*.\s*.Respondent', r'(\w+)\.*\s*Respondent', r'(\w+)\.*\s*Respondant']
			for regex_r in regexlist_respondant:
				rep_r = regex_r.replace("\W*","").replace("(\w.*)","").replace(".*(\w*)","").replace("\s.*","").replace("\W","").replace(".*?","").replace("Vs","")
				respondant = re.search(regex_r, header2, re.IGNORECASE)
				if respondant:
					respondante = re.compile(rep_r).sub("", respondant.group(0)).replace("Vs","").replace("..","")
					respondante = respondante[1:] if respondante[0].startswith(".") else respondante
					respondante = re.sub(r'\s+vs[\.]{0,1}\s*','',respondante, flags=re.I).strip()
					respondante = re.compile(re.escape('with'), re.IGNORECASE).sub('', respondante).strip()
					for r in ['respondent', 'respondant']:
						if r in respondante.lower():
							respondante = re.compile(r, re.IGNORECASE).split(respondante)[0]
					for r in ['vs', 'versus']:
						if r in respondante.lower():
							respondante = re.compile(r, re.IGNORECASE).split(respondante)[1]
					break
		else:
			splits = filename.split("-v-")
			appellante = splits[0].replace("-", " ")
			respondante = splits[1].replace("-", " ")

		regexlist_section = [r'\d{1,3}\s{0,2}[A-z]{0,1}\s{0,2}(\([\w]{1,2}\)){1,2}']
		for body in is_fa_de_body:
			for regex_sec in regexlist_section:
				sections_index1 = re.finditer(regex_sec, " ".join(body), re.IGNORECASE)
				for section in sections_index1:
					index = section.group(0)
					sections_index_list.add(str(index).strip())

		regexlist_section = [r'Section\s+\d{1,3}\s{0,2}[A-z]{0,1}\s{0,2}(\([\w]{1,2}\)){1,2}', r'Section\s+\d{1,3}-?[A-z]\s{0,2}(\([\w]{1,2}\)){0,2}', r'Section\s+\d{1,3}[A-z]{0,1}\s{0,2}(\([\w]{1,2}\)){0,2}', r'Rule\s+\d{1,3}\s{0,2}[A-z]{0,1}\s{0,2}(\([\w]{1,2}\)){1,2}', r'Rule\s+\d{1,3}-?[A-z]\s{0,2}(\([\w]{1,2}\)){0,2}', r'Rule\s+\d{1,3}[A-z]{0,1}\s{0,2}(\([\w]{1,2}\)){0,2}']
		for regex_sec in regexlist_section:
			sections_index1 = re.finditer(regex_sec, body1, re.IGNORECASE)
			if sections_index1:
				for section in sections_index1:
					index = section.group(0)
					index = re.compile(re.escape('section'), re.IGNORECASE).sub('', index)
					index = re.compile(re.escape('rule'), re.IGNORECASE).sub('', index)
					sections_index_list.add(str(index).strip())
			else: 
				sections_index1 = re.finditer(regex_sec, header2, re.IGNORECASE)
				if sections_index1:
					for section in sections_index1:
						index = section.group(0)
						index = re.compile(re.escape('section'), re.IGNORECASE).sub('', index)
						index = re.compile(re.escape('rule'), re.IGNORECASE).sub('', index)
						sections_index_list.add(str(index).strip())

		sections_index = ", ".join(sections_index_list)

		tag = overall_tag(is_fa_de_body)
		pos= pos_tag(tag)
		for word,tag in pos:
			if tag == "NNP":
				keywords.append(word.replace("XX",""))
		keywords = keywords[:10]
		keywords_to_show = ", ".join(keywords)

		header_data['filename'] = filename or "-"
		header_data['appellant'] = appellante or "-"
		header_data['respondant'] = respondante or "-"
		header_data['date_of_judgement'] = date_of_judgemente or "-"
		header_data['keyword'] = keywords_to_show or "-"
		header_data['appellant_by'] = appellant_by or "-"
		header_data['respondent_by'] = respondent_by or "-"
		header_data['assessment_year'] = assessment_year or "-"
		header_data['section_referred'] = section_referred or "-"
		header_data['assessee_by'] = assessee_by or "-"
		header_data['revenue_by'] = revenue_by or "-"
		header_data['courtname'] = courtname or "-"
		header_data['judges'] = judges or "-"
		header_data['civil_no'] = civil_no or "-"
		header_data['date_of_pronouncement'] = date_of_pronouncement or "-"
		header_data['date_of_hearing'] = date_of_hearing or "-"
		header_data['sections_index'] = sections_index or "-"

	except BaseException as e:
		print traceback.format_exc(e)
	return {'data': result, 'header_data': header_data}
