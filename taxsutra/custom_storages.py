from django.conf import settings
from boto.s3.connection import S3Connection
from storages.backends.s3boto import S3BotoStorage
from django.core.files.storage import get_storage_class

# By default the S3Connection is using s3.amazonaws.com
# as host, which only allow for S3 hosted in US. To allow
# EU hosts we need to change the DefaultHost with our own custom
# Connection Class.
class EUConnection(S3Connection):
    DefaultHost = "s3-us-west-2.amazonaws.com"

# We need a custom Storage Class for our Media files because we
# want to have seperate locations for Static and Media files.
class MediaStorage(S3BotoStorage):
    connection_class = EUConnection
    location = settings.MEDIAFILES_LOCATION

    def __init__(self, *args, **kwargs):
        super(MediaStorage, self).__init__(*args, **kwargs)
        self.local_storage = get_storage_class(
            "compressor.storage.CompressorFileStorage")()

    def save(self, name, content):
        name = super(MediaStorage, self).save(name, content)
        return name

# S3 storage backend that saves the files locally, too.
class CachedS3BotoStorage(S3BotoStorage):
    connection_class = EUConnection
    location = settings.STATICFILES_LOCATION
    
    def __init__(self, *args, **kwargs):
        super(CachedS3BotoStorage, self).__init__(*args, **kwargs)
        self.local_storage = get_storage_class(
            "compressor.storage.CompressorFileStorage")()

    def save(self, name, content):
        name = super(CachedS3BotoStorage, self).save(name, content)
        return name