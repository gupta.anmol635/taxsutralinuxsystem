#instructions
#install
sudo apt-get install nginx
sudo apt-get install uwsgi
sudo pip install uwsgi

#make softlinks for following
sudo ln -s /home/ubuntu/taxsutra/conf/uwsgi.conf /etc/init/uwsgi.conf #upstart
sudo ln -s /home/ubuntu/taxsutra/conf/django.ini /etc/uwsgi/sites/django.ini #uwsgi
Production=> sudo ln -s /home/ubuntu/taxsutra/conf/nginx.conf /etc/nginx/sites-enabled/nginx.conf #nginx for production
Staging=> sudo ln -s /home/ubuntu/taxsutra/conf/staging.nginx.conf /etc/nginx/sites-enabled/nginx.conf #nginx for staging
sudo service nginx restart
sudo service uwsgi restart

#tutorial
#for nginx and uwsgi configuration
https://gist.github.com/evildmp/3094281
#for upstart configuration
https://www.digitalocean.com/community/tutorials/how-to-serve-django-applications-with-uwsgi-and-nginx-on-ubuntu-14-04

#tests

#without nginx
uwsgi --http :8000 --home /home/ubuntu/venv --chdir /home/ubuntu/taxsutra/ -w taxsutraapp.wsgi
#if another process is running on 8000 kill that by below command
#sudo fuser -k 8000/tcp


#with nginx
uwsgi --socket /tmp/uwsgi.sock --wsgi-file /home/ubuntu/taxsutra/taxsutraapp/wsgi.py --chmod-socket=666

#with nginx and django.ini
uwsgi --ini /home/ubuntu/taxsutra/conf/django.ini 


#crontab
create cronlog folder
crontab -e
#Add
0 * * * * flock -n /tmp/fc.lockfile /home/ubuntu/venv/bin/python /home/ubuntu/taxsutra/manage.py feeds_cron >> /home/ubuntu/cronlog/feeds_cron.log
45 * * * * flock -n /tmp/lc.lockfile /home/ubuntu/venv/bin/python /home/ubuntu/taxsutra/manage.py letsventure_cron >> /home/ubuntu/cronlog/letsventure_cron.log
# RUN /home/ubuntu/venv/bin/python /home/ubuntu/taxsutra/manage.py cricket_cron >> /home/ubuntu/cronlog/cricket_cron.log in TMUX




#migrations failed:

git reset head --hard <commit-id> (revert to previous commit)
python manage.py migrate --fake
git pull
python manage.py makemigrations engine
python manage.py migrate

#Every reboot check:

Nginx running
uwsgi running
memcache running

#memcache

sudo apt-get install memcached
sudo apt-get install python-memcache
